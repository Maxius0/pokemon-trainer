# Pokemon Trainer

This is the fourth assignment in the frontend module of Noroff's full stack development course. See assignment description [here](project/Angular_Pokemon_Trainer.pdf).

The component tree for this project is found [here](project/pokemon_trainer_component_tree.pdf).

## Install

Clone or download repository. For more information on how to run Angular apps, see [angular.io](https://angular.io/cli/run).

## Usage

The app is deployed live at

<!-- [Heroku](https://tranquil-scrubland-83982.herokuapp.com/) -->

## Maintainers

Frosti Hallfridarson
https://gitlab.com/frostih

Marius Samson
https://gitlab.com/Maxius0
