export interface Catalogue {
  name: string;
  id: number;
  pictureURL: string;
}
