import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const ID_KEY = 'trainer-id';
const NAME_KEY = 'trainer-name';
const POKEMON_KEY = 'pokemon-name';

const URL = environment.trainerAPI_URL;
const KEY = environment.trainerAPI_KEY;

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _id = '';
  private _name = '';
  private _pokemonIds: string[] = [];
  private _pokemonNames: string[] = [];
  private _pokemonPictureURL: string[] = [];

  get id(): string {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get pokemonIds(): string[] {
    return this._pokemonIds;
  }

  get pokemonNames(): string[] {
    return this._pokemonNames;
  }

  get pokemonPictureURL(): string[] {
    return this._pokemonPictureURL;
  }

  // Adds or Removes pokemoID to local storage, depends on state
  set pokemonsIds(id: string) {
    const tempArray = this._pokemonIds;
    if (tempArray.includes(id)) {
      for (let i = 0; i < tempArray.length; i++) {
        if (tempArray[i] === id) {
          tempArray.splice(i, 1);
        }
      }
    } else {
      tempArray.push(id);
    }
    this._pokemonIds = tempArray;
  }

  
  // Adds or Removes pokemoName to local storage, depends on state
  set pokemonsNames(pokemonName: string) {
    const tempArray = this._pokemonNames;
    if (tempArray.includes(pokemonName)) {
      for (let i = 0; i < tempArray.length; i++) {
        if (tempArray[i] === pokemonName) {
          tempArray.splice(i, 1);
        }
      }
    } else {
      tempArray.push(pokemonName);
    }
    this._pokemonNames = tempArray;
    //localStorage.setItem(POKEMON_KEY, JSON.stringify(this.pokemonNames));
  }

  
  // Adds or Removes pokemon picture URL to local storage, depends on state
  set pokemonsPictureURL(pictureURL: string) {
    const tempArray = this._pokemonPictureURL;
    if (tempArray.includes(pictureURL)) {
      for (let i = 0; i < tempArray.length; i++) {
        if (tempArray[i] === pictureURL) {
          tempArray.splice(i, 1);
        }
      }
    } else {
      tempArray.push(pictureURL);
    }
    this._pokemonPictureURL = tempArray;
  }

  set id(id: string) {
    localStorage.setItem(ID_KEY, String(id));
    this._id = id;
  }

  set name(name: string) {
    localStorage.setItem(NAME_KEY, name);
    this._name = name;
  }

  constructor(private http: HttpClient) {
    this._name = localStorage.getItem(NAME_KEY) || '';
    this._id = localStorage.getItem(ID_KEY) || '';
    if (localStorage.getItem(POKEMON_KEY) != null) {
      this._pokemonNames =
        JSON.parse(localStorage.getItem(POKEMON_KEY) || '{}') || [];
    }
  }

  loggedIn(): boolean {
    return localStorage.getItem(NAME_KEY) !== null;
  }

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': KEY,
    });
  }

  trainerLookup(trainerName: string): Observable<any> {
    return this.http.get(`${URL}?username=${trainerName}`);
  }

  register(trainerName: string): Observable<any> {
    const headers = this.createHeaders();
    const trainer: Trainer = {
      username: trainerName,
      pokemon: [],
    };
    return this.http.post(URL, JSON.stringify(trainer), { headers });
  }

  updatePokemons(newPokemon: string): Observable<any> {
    const headers = this.createHeaders();
    const pokemons = this.pokemonNames;

    return this.http.patch(
      `${URL}/${this.id}`,
      JSON.stringify({ pokemon: pokemons }),
      {
        headers,
      }
    );
  }
}
