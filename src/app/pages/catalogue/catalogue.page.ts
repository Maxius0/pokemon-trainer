import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Catalogue } from 'src/app/models/catalogue.model';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

// First 649 pokemons, as they have good quality images from dream-world, the rest don't have dream world images.
const URL = 'https://pokeapi.co/api/v2/pokemon?limit=649';

// Returns id (xx) of pokemon from
// URL https://pokeapi.co/api/v2/pokemon/XX/
function ExtractId(pokemonURL: string) {
  const stringId = parseInt(pokemonURL.slice(34).split('/')[0]);
  return stringId;
}

// Returns a string with URL to picture of given pokemon ID
function PokemonPictureURL(pokemonId: number) {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemonId}.svg`;
}

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css'],
})
export class CataloguePage implements OnInit, OnDestroy {
  // DI - Dependency Injection
  constructor(
    private http: HttpClient,
    private router: Router,
    private trainerService: TrainerService
  ) {}

  public selectedPokemons: Catalogue[] = [];
  public catalogue: Catalogue[] | null = null;

  public trainerPokemonsIds = this.trainerService.pokemonIds;
  public trainerPokemonsNames = this.trainerService.pokemonNames;
  public trainerPokemonsPictureURLs = this.trainerService.pokemonPictureURL;

  handleClickTrainerPage() {
    this.router.navigateByUrl('/trainer');
  }

  // Adds pokemon to local storage
  pokemonSelected(pokemonId: number): void {
    if (this.catalogue !== null) {
      const currentPokemon = this.catalogue[pokemonId - 1];
      this.trainerService.pokemonsIds = JSON.stringify(currentPokemon.id);
      this.trainerService.pokemonsPictureURL = currentPokemon.pictureURL;
      this.trainerService.pokemonsNames = currentPokemon.name;

      this.trainerService.updatePokemons(currentPokemon.name).subscribe({
        next: (response: any) => {
          console.log(response);
        },
      });
    }
  }

  pokemonHasBeenSelected(checkId: number): boolean {
    for (let id of this.trainerService.pokemonIds) {
      if (parseInt(id) === checkId) {
        return true;
      }
    }
    return false;
  }

  //  Removes pokemon from local storage
  pokemonDeselected(pokemonId: number): void {
    if (this.catalogue !== null) {
      const currentPokemon = this.catalogue[pokemonId - 1];

      this.trainerService.pokemonsIds = JSON.stringify(currentPokemon.id);
      this.trainerService.pokemonsPictureURL = currentPokemon.pictureURL;
      this.trainerService.pokemonsNames = currentPokemon.name;
    }
  }

  // Adds pokemons from API to catalogue: Catalogue[]
  handleResponse(response: any) {
    const { results = [] } = response;
    if (results.length === 0) {
      alert('No Pokemons found.');
      return;
    }
    this.catalogue = [];
    for (const pokemon of results) {
      this.catalogue.push({
        name: pokemon.name,
        id: ExtractId(pokemon.url),
        pictureURL: PokemonPictureURL(ExtractId(pokemon.url)),
      });
    }
  }

  // API request
  ngOnInit() {
    if (sessionStorage.getItem('catalogue') === null) {
      this.http
        .get(URL) // Request
        .subscribe({
          next: (response: any) => {
            // .then()
            this.handleResponse(response);
            sessionStorage.setItem('catalogue', JSON.stringify(this.catalogue));
          },
          error: (error) => {
            // .catch()
            console.log(error.message);
          },
        });
    } else {
      this.catalogue = JSON.parse(sessionStorage.getItem('catalogue') || '{}');
    }
  }

  // Deletes API request
  ngOnDestroy() {}
}
