import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage implements OnInit {
  constructor(private router: Router, private trainerService: TrainerService) {
    if (this.trainerService.loggedIn()) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  ngOnInit(): void {}

  onLoginSubmit(form: NgForm): void {
    const { trainerName } = form.value;
    this.trainerService.trainerLookup(trainerName).subscribe({
      next: (response: any) => {
        if (JSON.stringify(response) !== '[]') {
          // login case
          this.trainerService.id = response[0].id;
          this.trainerService.name = response[0].username;
          this.router.navigateByUrl('/catalogue');
        } else {
          // register case
          this.trainerService.register(trainerName).subscribe({
            next: (response: any) => {
              this.trainerService.id = response.id;
              this.trainerService.name = response.username;
              this.router.navigateByUrl('/catalogue');
            },
          });
        }
      },
    });
  }
}
