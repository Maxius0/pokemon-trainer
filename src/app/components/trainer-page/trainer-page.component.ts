import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Catalogue } from 'src/app/models/catalogue.model';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css'],
})
export class TrainerPageComponent {
  // DI - Dependency Injection
  constructor(private router: Router, private trainerService: TrainerService) {}

  public trainerPokemonsIds = this.trainerService.pokemonIds;
  public trainerPokemonsNames = this.trainerService.pokemonNames;
  public trainerPokemonsPictureURLs = this.trainerService.pokemonPictureURL;


  
  handleClickTrainerPage() {
    this.router.navigateByUrl('/catalogue');
  }

  // Romoves selected pokemons from local storage
  pokemonSelected(pokemonId: string, pokemonURL: string, pokemonName: string): void {
      this.trainerService.pokemonsIds = pokemonId;
      this.trainerService.pokemonsPictureURL = pokemonURL;
      this.trainerService.pokemonsNames = pokemonName;
    
  }
}
