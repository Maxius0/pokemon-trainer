import { Component, OnInit } from '@angular/core';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  get trainer(): string {
    return this.trainerService.name;
  }

  constructor(private readonly trainerService: TrainerService) {}

  logout(): void {
    localStorage.removeItem('trainer-name');
    localStorage.removeItem('trainer-id');
    localStorage.removeItem('pokemon-name');
  }

  ngOnInit(): void {}
}
